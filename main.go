package main

import (
	"API/controller"
	"API/database"
	handler "API/handlers"
	"API/models"
)

func main() {
	db := database.InitConnection()
	postModel := models.InitPostModel(db)
	filePath := "/Users/macbook/Documents/API/log/api.log"
	postService := controller.InitPostService(postModel, filePath)
	handler.Handle(postService)
}
