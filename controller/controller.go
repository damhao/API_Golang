package controller

import (
	"API/models"
	"API/response"
	"API/utils"
	"encoding/json"
	"errors"
	"io/ioutil"
	"time"

	"API/types"
	"fmt"
	"net/http"
	"strconv"
)

type PostService struct {
	// api.PostInterface
	FilePath  string
	PostModel *models.PostModel
}

func InitPostService(postModel *models.PostModel, filePath string) *PostService {
	return &PostService{
		PostModel: postModel,
		FilePath:  filePath,
	}
}

func (p *PostService) GetPostByID(w http.ResponseWriter, h *http.Request) {
	now := time.Now()

	param := h.URL.Query().Get("id")

	id, err := strconv.Atoi(param)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, err)
		return
	}

	post, err := p.PostModel.GetPostByID(id)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, err)
		return
	}

	err = utils.ExportFile(fmt.Sprintf("METHOD : %s\nACTION : GetPostByID\nINPUT : id = %d\nOUTPUT : %+v\nCREATED_TIME : %v\n\n", "GET", id, post, now), p.FilePath)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, errors.New("error when writing to file "))
		return
	}

	response.ResponsePost(w, 200, post)

}
func (p *PostService) GetAllPosts(w http.ResponseWriter, h *http.Request) {
	now := time.Now()
	pageStr := h.URL.Query().Get("page")
	page, err := strconv.Atoi(pageStr)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, err)
		return
	}

	limitStr := h.URL.Query().Get("limit")
	limit, err := strconv.Atoi(limitStr)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, err)
		return
	}

	posts, err := p.PostModel.GetAllPosts(page, limit)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, err)
		return
	}

	err = utils.ExportFile(fmt.Sprintf("METHOD : %s\nACTION : GetAllPosts\nINPUT : page = %d, limit = %d\nOUTPUT : %+v\nCREATED_TIME : %v\n\n", "GET", page, limit, posts, now), p.FilePath)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, errors.New("error when writing to file "))
		return
	}

	response.ResponseWithJson(w, http.StatusOK, posts)
	//w.Write()

}
func (p *PostService) DeletePostByID(w http.ResponseWriter, h *http.Request) {
	now := time.Now()
	param := h.URL.Query().Get("id")

	id, err := strconv.Atoi(param)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, err)
		return
	}

	err = p.PostModel.DeletePostByID(id)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, err)
		return
	}
	err = utils.ExportFile(fmt.Sprintf("METHOD : %s\nACTION : DeletePostByID\nINPUT : id = %d\nOUTPUT :\nCREATED_TIME : %v\n\n", "DELETE", id, now), p.FilePath)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, errors.New("error when writing to file "))
		return
	}
	response.ResponseWithJson(w, http.StatusOK, "deleted successful")
}

func (p *PostService) CreatePost(w http.ResponseWriter, h *http.Request) {
	now := time.Now()
	body, err := ioutil.ReadAll(h.Body)
	if err != nil {
		response.ResponseError(w, 500, err)
		return
	}

	var post types.PostTable
	err = json.Unmarshal(body, &post)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, err)
		return
	}

	err = p.PostModel.CreatePost(post)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, err)
		return
	}
	err = utils.ExportFile(fmt.Sprintf("METHOD : %s\nACTION : CreatePost\nINPUT : post = %+v\nOUTPUT :\nCREATED_TIME : %v\n\n", "POST", post, now), p.FilePath)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, errors.New("error when writing to file "))
		return
	}

	response.ResponseWithJson(w, http.StatusOK, "created successful")
}
func (p *PostService) UpdatePost(w http.ResponseWriter, h *http.Request) {
	now := time.Now()
	body, err := ioutil.ReadAll(h.Body)
	if err != nil {
		response.ResponseError(w, 500, err)
		return
	}

	var post types.PostTable
	err = json.Unmarshal(body, &post)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, err)
		return
	}

	err = p.PostModel.UpdatePost(post)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, err)
		return
	}

	err = utils.ExportFile(fmt.Sprintf("METHOD : %s\nACTION : UpdatePost\nINPUT : post = %+v\nOUTPUT :\nCREATED_TIME : %v\n\n", "PUT", post, now), p.FilePath)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, errors.New("error when writing to file "))
		return
	}

	response.ResponseWithJson(w, http.StatusOK, "update successful")

}
func (p *PostService) GetPostByTitle(w http.ResponseWriter, h *http.Request) {
	now := time.Now()
	body, err := ioutil.ReadAll(h.Body)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, err)
		return
	}

	var title string

	err = json.Unmarshal(body, &title)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, err)
		return
	}

	posts, err := p.PostModel.GetPostByTitle(title)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, err)
		return
	}

	err = utils.ExportFile(fmt.Sprintf("METHOD : %s\nACTION : GetPostByTitle\nINPUT : title = %s\nOUTPUT : %+v\nCREATED_TIME : %v\n\n", "GET", title, posts, now), p.FilePath)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, errors.New("error when writing to file "))
		return
	}

	response.ResponseWithJson(w, 200, posts)

}

func (p *PostService) GetPostByTopic(w http.ResponseWriter, h *http.Request) {
	now := time.Now()
	body, err := ioutil.ReadAll(h.Body)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, err)
		return
	}

	var topic string

	err = json.Unmarshal(body, &topic)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, err)
		return
	}

	posts, err := p.PostModel.GetPostByTopic(topic)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, err)
		return
	}

	err = utils.ExportFile(fmt.Sprintf("METHOD : %s\nACTION : GetPostByTopic\nINPUT : topic = %s\nOUTPUT : %+v\nCREATED_TIME : %v\n\n", "GET", topic, posts, now), p.FilePath)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, errors.New("error when writing to file "))
		return
	}

	response.ResponseWithJson(w, 200, posts)

}
