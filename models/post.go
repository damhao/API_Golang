package models

import (
	"API/types"
	"errors"
	"fmt"

	"gorm.io/gorm"
)

type PostModel struct {
	DB   *gorm.DB
	Post types.PostTable
}

func InitPostModel(db *gorm.DB) *PostModel {
	return &PostModel{
		DB: db,
	}
}

func (p *PostModel) GetPostByID(id int) (types.PostTable, error) {

	var post types.PostTable
	err := p.DB.Where("id = ?", id).First(&post).Error
	if err != nil {
		return types.PostTable{}, err
	}

	return post, nil

}

func (p *PostModel) GetAllPosts(page, limit int) ([]types.PostTable, error) {

	var posts []types.PostTable

	err := p.DB.Limit(limit).Offset(page * limit).Find(&posts).Error
	if err != nil {
		return nil, err
	}

	return posts, nil

}

func (p *PostModel) DeletePostByID(id int) error {

	var post = types.PostTable{
		ID: id,
	}
	result := p.DB.Delete(&post)
	if result.Error != nil {
		return result.Error
	}

	rowAffected := result.RowsAffected
	if rowAffected == 0 {
		return errors.New("delete failed")
	}

	return nil

}

func (p *PostModel) CreatePost(post types.PostTable) error {

	result := p.DB.Create(&post)
	if result.Error != nil {
		return result.Error
	}

	rowAffected := result.RowsAffected
	if rowAffected == 0 {
		return errors.New("created failed")
	}

	return nil

}
func (p *PostModel) UpdatePost(post types.PostTable) error {
	result := p.DB.Updates(&post)
	if result.Error != nil {
		return result.Error
	}

	rowAffected := result.RowsAffected
	if rowAffected == 0 {
		return errors.New("update failed")
	}

	return nil

}
func (p *PostModel) GetPostByTitle(title string) ([]types.PostTable, error) {

	var posts []types.PostTable
	err := p.DB.Where("title like ? ", fmt.Sprint("%", title, "%")).Find(&posts).Error

	if err != nil {
		return nil, err
	}

	return posts, nil

}

func (p *PostModel) GetPostByTopic(topic string) ([]types.PostTable, error) {

	var posts []types.PostTable
	err := p.DB.Where("topic = ?", topic).Find(&posts).Error

	if err != nil {
		return nil, err
	}

	return posts, nil

}
